import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.ateam.springboot.Booking;
import com.ateam.springboot.BookingRepository;
import com.ateam.springboot.BookingService;

/**
 * Created by alessiod on 30/03/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class BookingServiceTest {

    @InjectMocks BookingService bookingService;

    @Mock BookingRepository bookingRepository;


    @Test public void saveTest() throws Exception {
        Booking booking = new Booking();
        bookingService.save(booking);
        Mockito.verify(bookingRepository).save(Mockito.eq(booking));
    }

    @Test public void closeTest() throws Exception {

        Long valueId = 1L;
        Booking booking = new Booking();
        Mockito.when(bookingRepository.getOne(1L)).thenReturn(booking);

        bookingService.close(valueId);

        Mockito.verify(bookingRepository).save(Mockito.eq(booking));
    }

    @Test public void priceTest3hours() throws Exception {

        int number = bookingService.priceForHours("15:00","12:00");

        Assert.assertEquals(5,number);
    }


    @Test public void priceTest8hours() throws Exception {

        int number = bookingService.priceForHours("18:00","10:00");

        Assert.assertEquals(15,number);
    }

}
