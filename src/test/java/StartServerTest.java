import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;
import java.util.Map;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ateam.springboot.StartServer;

/**
 * Created by alessiod on 29/03/17.
 */
public class StartServerTest {

    @BeforeClass
    public static void onceExecutedBeforeAll() {
        new StartServer().run();
    }

    @Test
    public void testListino() throws IOException {
        URL home = new URL("http://localhost:8080/home");
        HttpURLConnection connection = (HttpURLConnection) home.openConnection();
        connection.setRequestMethod("GET");
        String result = new BufferedReader(new InputStreamReader(connection.getInputStream())).lines().collect(Collectors.joining("\n"));
        Assert.assertTrue(result.contains("Listino Prezzi"));
    }

    @Test
    public void testRegistrazione() throws IOException, InterruptedException {
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("name","Mario Bianchi");
        params.put("type","City Bike");

        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");


        URL open = new URL("http://localhost:8080/open");
        HttpURLConnection connection = (HttpURLConnection) open.openConnection();
        connection.setDoOutput( true );
        connection.setRequestMethod("POST");
        connection.getOutputStream().write(postDataBytes);
        connection.getInputStream();
        Assert.assertEquals(200,connection.getResponseCode());

        String result = new BufferedReader(new InputStreamReader(connection.getInputStream())).lines().collect(Collectors.joining("\n"));
        Assert.assertTrue(result.contains("Mario Bianchi"));
        Assert.assertTrue(result.contains("City Bike"));

    }
}
