package com.ateam.springboot;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

    @Autowired
    BookingService bookingService;

    @RequestMapping("/index")
    public String index(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        getListItems(model);
        return "index";
    }

    @RequestMapping("/home")
    public String home(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        getListItems(model);
        return "public";
    }

    private void getListItems(Model model) {
        List<Booking> bookingList = bookingService.findOpened();
        model.addAttribute("bookings",bookingList);

        List<Booking> bookingListClosed = bookingService.findClosed();
        model.addAttribute("bookingsClosed",bookingListClosed);
    }

    @RequestMapping("/open")
    public String open(Model model, @ModelAttribute Booking booking) {
        bookingService.save(booking);
        return "redirect:index#pratiche";
    }


    @RequestMapping("/close")
    public String close(Model model,
            @RequestParam(value="id", required=false) Long id
            ) {

        bookingService.close(id);
        return "redirect:index#pratiche";
    }

    @RequestMapping("/list")
    public String list(Model model) {
        return "redirect:index#pratiche";
    }

}