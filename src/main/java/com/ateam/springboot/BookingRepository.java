package com.ateam.springboot;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by alessiod on 28/03/17.
 */
public interface BookingRepository extends JpaRepository<Booking, Long> {

    @Query("SELECT b FROM Booking b WHERE b.open = true")
    public List<Booking> findOpened();

    @Query("SELECT b FROM Booking b WHERE b.open = false")
    public List<Booking> findClosed();

}
