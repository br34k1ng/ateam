package com.ateam.springboot;

import java.util.List;

/**
 * Created by alessiod on 30/03/17.
 */
public class BookingService {

    BookingRepository bookingRepository;

    public void setBookingRepository(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    public Booking save(Booking booking) {
        return bookingRepository.save(booking);
    }

    public Booking get(Long id) {
        return bookingRepository.getOne(id);
    }

    public List<Booking> findOpened() {
        return bookingRepository.findOpened();
    }

    public List<Booking> findClosed() {
        return bookingRepository.findClosed();
    }

    public void close(Long id) {
        Booking booking = bookingRepository.getOne(id);
        booking.setOpen(false);
        bookingRepository.save(booking);
    }

    public  int priceForHours(String timeString1 , String timeString2)
    {
        String[] fractions1;
        fractions1 = timeString1.split(":");
        String[] fractions2=timeString2.split(":");
        Integer hours1=Integer.parseInt(fractions1[0]);
        Integer hours2=Integer.parseInt(fractions2[0]);
        Integer minutes1=Integer.parseInt(fractions1[1]);
        Integer minutes2=Integer.parseInt(fractions2[1]);
        int hourDiff=hours1-hours2;
        int minutesDiff=hours1-hours2;

        if(hourDiff>5)
        {
            return 15;
        }else
        {
            return 5;
        }
    }

}
