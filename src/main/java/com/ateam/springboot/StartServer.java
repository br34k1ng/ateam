package com.ateam.springboot;

import org.springframework.boot.SpringApplication;

/**
 * Created by alessiod on 29/03/17.
 */
public class StartServer {
    public void run() {
        SpringApplication.run(WebApplication.class);
    }
}
