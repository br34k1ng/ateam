<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<title>Vecchia Zia</title>
		<meta name="description" content="Worthy a Bootstrap-based, Responsive HTML5 Template">
		<meta name="author" content="htmlcoder.me">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="images/favicon.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700,300&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>

		<!-- Bootstrap core CSS -->
		<link href="bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="css/animations.css" rel="stylesheet">

		<!-- Worthy core CSS file -->
		<link href="css/style.css" rel="stylesheet">

		<!-- Custom css -->
		<link href="css/custom.css" rel="stylesheet">
	</head>

	<body class="no-trans">
		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop"><i class="icon-up-open-big"></i></div>

		<!-- header start -->
		<!-- ================ -->
		<header class="header fixed clearfix navbar navbar-fixed-top">
			<div class="container">
				<div class="row">
					<div class="col-md-4">

						<!-- header-left start -->
						<!-- ================ -->
						<div class="header-left clearfix">

							<!-- logo -->
							<!--div class="logo smooth-scroll">
								<a href="#banner"><img id="logo" src="images/gifruota.gif"  alt="Worthy" style="height:60px;"></a>
							</div>

							<!-- name-and-slogan -->
							<div class="site-name-and-slogan smooth-scroll">
								<div class="site-name"><a href="#banner">A-Team Mobility</a></div>

							</div>

						</div>
						<!-- header-left end -->

					</div>
					<div class="col-md-8">

						<!-- header-right start -->
						<!-- ================ -->
						<div class="header-right clearfix">

							<!-- main-navigation start -->
							<!-- ================ -->
							<div class="main-navigation animated">

								<!-- navbar start -->
								<!-- ================ -->
								<nav class="navbar navbar-default" role="navigation">
									<div class="container-fluid">

										<!-- Toggle get grouped for better mobile display -->
										<div class="navbar-header">
											<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
												<span class="sr-only">Toggle navigation</span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
											</button>
										</div>

										<!-- Collect the nav links, forms, and other content for toggling -->
										<div class="collapse navbar-collapse scrollspy smooth-scroll" id="navbar-collapse-1">
											<ul class="nav navbar-nav navbar-right">
												<li class="active"><a href="#banner">Home</a></li>
												<li class="active"><a href="#listino">Listino Prezzi</a></li>
												<li class="active"><a href="#prenota">Registra Noleggio</a></li>
												<li class="active"><a href="#pratiche">Elenco Pratiche</a></li>
											</ul>
										</div>

									</div>
								</nav>
								<!-- navbar end -->

							</div>
							<!-- main-navigation end -->

						</div>
						<!-- header-right end -->

					</div>
				</div>
			</div>
		</header>
		<!-- header end -->

		<!-- banner start -->
		<!-- ================ -->
		<div id="banner" class="banner">
			<div class="banner-image"></div>
			<div class="banner-caption">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 object-non-visible" data-animation-effect="fadeIn">
							<h1 class="text-center">Vecchia zia <br/><span>Bike rental</span></h1>
							<p class="lead text-center">Noleggia da noi la tua bici, e scopri la <a href="http://www.vivipavia.it/site/home/luoghi/luoghi-di-cultura/certosa-di-pavia.html">Certosa</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>


<div id="listino">
	<div class="container">
		<div  style="padding-top:40px; font-size:25px;" >Listino Prezzi</div>
		<h1><label class="label label-primary">3 euro all'ora</label></h1>
		<h1><label class="label label-primary">15 euro giornata (pi&ugrave; di 5 ore)</label></h1>
	</div>

</div>

<div id="prenota">
	<div class="container">
			<div  style="padding-top:40px; padding-bottom:20px; font-size:25px;" >Registra noleggio</div>

			<div>
								<form role="form" id="footer-form" action="open">
									<div class="form-group has-feedback">
										<span class="label label-primary">Tipo</span>
										<select class="form-control" id="type" value="Mountain Bike" name="type">
										<option value="Mountain Bike">Mountain Bike</option>
										<option value="City Bike">City Bike</option>
										<option value="Tandem">Tandem</option>
										<option value="Bici Bambino">Bici Bambino</option>
										</select>
									</div>
									<div class="form-group has-feedback">
										<span class="label label-primary">Nome e Cognome</span>
										<input type="text" class="form-control" id="name" placeholder="Nome e Cognome" name="name" required>
										<!--<i class="fa fa-user form-control-feedback"></i>-->
									</div>
									<div class="form-group has-feedback">
										<span class="label label-primary">Numero di telefono</span>
										<input type="text" class="form-control" id="telefono" placeholder="Numero di telefono" name="phone" required>
										<!--<i class="fa fa-envelope form-control-feedback"></i>-->
									</div>
									<div class="form-group has-feedback">
										<span class="label label-primary">Ora prelievo</span>
										<input type="time" class="form-control" id="oraPrelievo" placeholder="Ora prelievo" name="time" required>
										<!--<i class="fa fa-pencil form-control-feedback"></i>-->
									</div>
									<div class="form-group has-feedback">
                                        <span class="label label-primary">Data prelievo</span>
                                        <input type="date" class="form-control" id="dataPrelievo" placeholder="Data prelievo" name="dateStart" required>
                                        <!--<i class="fa fa-pencil form-control-feedback"></i>-->
                                    </div>
									<div class="form-group has-feedback">
										<span class="label label-primary">Ora riconsegna</span>
										<input type="time" class="form-control" id="oraRitiro" placeholder="Ora ritiro" name="timeEnd" required>
										<!--<i class="fa fa-pencil form-control-feedback"></i>-->
									</div>

                                    <div class="form-group has-feedback">
                                        <span class="label label-primary">Data riconsegna</span>
                                        <input type="date" class="form-control" id="dataRitiro" placeholder="Data ritiro" name="dateEnd" required>
                                        <!--<i class="fa fa-pencil form-control-feedback"></i>-->
                                    </div>
									<div class="form-group has-feedback">
										<span class="label label-primary">ID documento</span>
										<input type="text" class="form-control" id="documento" placeholder="ID documento" name="documentId" required>
									</div>
									<input type="submit" value="Registra" class="btn btn-default">
								</form>
							</div>
		</div>

<div id="pratiche">
	<div class="container">
			<div  style="padding:40px; font-size:25px;" >Elenco Pratiche aperte</div>
			<div class="table-responsive">
				<table class="table table-stripped">
					<tr>
						<td>
							Nome e Cognome
						</td>
						<td>
							Telefono
						</td>
						<td>
							Tipo di bici
						</td>
						<td>
							Orario prelievo
						</td>
						<td>
                            Data prelievo
                        </td>
						<td>
							Orario ritiro
						</td>
						<td>
                            Data ritiro
                        </td>
						<td>
							Documento
						</td>
						<td></td>
					</tr>


                    <c:forEach items="${bookings}" var="b">
                    <tr>
                    						<td>
                    							${b.name}
                    						</td>
                    						<td>
                    							${b.phone}
                    						</td>
                    						<td>
                    							${b.type}
                    						</td>
                    						<td>
                    							${b.time}
                    						</td>
                    						<td>
                                                ${b.dateStart}
                                            </td>
                    						<td>
                    							${b.timeEnd}
                    						</td>
                    						<td>
                                                                                            ${b.dateEnd}
                                                                                        </td>
                    						<td>
                    							${b.documentId}
                    						</td>
                    						<td><a href="close?id=${b.id}">Chiudi pratica</a>
                    					</tr>
                    </c:forEach>
				</table>
			</div>
	</div>
</div>
<div id="pratiche">
	<div class="container">
			<div  style="padding:40px; font-size:25px;" >Elenco Pratiche chiuse</div>
			<div class="table-responsive">
				<table class="table table-stripped">
					<tr>
						<td>
							Nome e Cognome
						</td>
						<td>
							Telefono
						</td>
						<td>
							Tipo di bici
						</td>
						<td>
                        							Orario prelievo
                        						</td>
                        						<td>
                                                    Data prelievo
                                                </td>
                        						<td>
                        							Orario ritiro
                        						</td>
                        						<td>
                                                    Data ritiro
                                                </td>
						<td>
							Documento
						</td>

					</tr>


                    <c:forEach items="${bookingsClosed}" var="b">
                    <tr>
                    						<td>
                    							${b.name}
                    						</td>
                    						<td>
                    							${b.phone}
                    						</td>
                    						<td>
                    							${b.type}
                    						</td>
                    						<td>
                    							${b.time}
                    						</td>
                    						<td>
                                                                                            ${b.dateStart}
                                                                                        </td>
                                                                						<td>
                                                                							${b.timeEnd}
                                                                						</td>
                                                                						<td>
                                                                                                                                        ${b.dateEnd}
                                                                                                                                    </td>
                    						<td>
                    							${b.documentId}
                    						</td>

                    					</tr>
                    </c:forEach>
				</table>
			</div>
	</div>
</div>

		<!-- banner end -->

		<footer id="footer">

			<!-- .footer start -->
			<!-- ================ -->
			<div class="footer section">
				<div class="container">
					<h1 class="title text-center" id="contact"></h1>
					<div class="space"></div>
					<div class="row">
						<div class="col-sm-6">
							<div class="footer-content">

							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- .footer end -->

			<!-- .subfooter start -->
			<!-- ================ -->
			<div class="subfooter">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<p class="text-center">Copyright &#169; 2017 A-Team</p>
						</div>
					</div>
				</div>
			</div>
			<!-- .subfooter end -->

		</footer>
		<!-- footer end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster
		================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="plugins/jquery.min.js"></script>
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="plugins/modernizr.js"></script>

		<!-- Isotope javascript -->
		<script type="text/javascript" src="plugins/isotope/isotope.pkgd.min.js"></script>

		<!-- Backstretch javascript -->
		<script type="text/javascript" src="plugins/jquery.backstretch.min.js"></script>

		<!-- Appear javascript -->
		<script type="text/javascript" src="plugins/jquery.appear.js"></script>

		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="js/custom.js"></script>

	</body>

</html>
